#ifndef UNITTEST_H
#define UNITTEST_H

#include <cassert>
#include "BruteForceMedian.h"
#include "Median.h"

class UnitTest
{
    public:
        UnitTest(int* testArray, unsigned long int arraySize, int answer_bfm, int answer_mdn);
        ~UnitTest();
        int getBruteForceMedian();
        int getMedian();
        bool testBruteForceMedian(void);
        bool testBruteForceMedianOpCounter(void);
        bool testMedian(void);
        bool testMedianOpCounter(void);

    protected:

    private:
        int* testArray;
        unsigned long int arraySize;
        int answer_bfm,
            answer_mdn,
            result_bfm,
            result_mdn,
            bo_bfm,
            bo_mdn;
};

#endif // UNITTEST_H
