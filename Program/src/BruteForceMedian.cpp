#include "BruteForceMedian.h"

BruteForceMedian::BruteForceMedian(int* problemArray, long unsigned int arraySize) {
    this->problemArray = problemArray;
    this->basicOperationCounter = 0;
    this->problemSize = arraySize;
}

BruteForceMedian::~BruteForceMedian(void) {
    //dtor
}

int BruteForceMedian::getMedian(void) {
    // The algorithm sorts arrayA[0..n-1] by improved bubble sort
    // Input: An array A[0..n-1] of orderable elements
    // Output: Array A[0..n-1] sorted in ascending order

    unsigned long int
        i,
        j,
        indexMiddle,
        numsmaller,
        numequal;

    // Returns the median value in a given array A of n numbers. This is
    // the kth element, where k=n/2, if the array was sorted.
    // Following calculation is to transform array size to array index.
    indexMiddle = ceil(this->problemSize / 2.0);
    for (i = 0; i < this->problemSize; ++i) {
        numsmaller = 0; // How many elements are smaller than A[i]
        numequal = 0; // How many elements are equal to A[i]
        for (j = 0; j < this->problemSize; ++j) {
            if (this->problemArray[j] < this->problemArray[i]) {
                numsmaller++;
            } else {
                if (this->problemArray[j] == this->problemArray[i]) {
                    numequal++;
                }
            }
            ++this->basicOperationCounter;
        }

        if ((numsmaller < indexMiddle)
            && (indexMiddle <= (numsmaller + numequal))) {
            return this->problemArray[i];
        }
    }

    return -1;
}

long int BruteForceMedian::getBasicOpCount(void) {
    return this->basicOperationCounter;
}
