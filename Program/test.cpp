#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <ctime>
#include <cassert>

#include "BruteForceMedian.h"

using namespace std;

int unit_test();

int unit_test() {
    int testArray[] = {1,2,3,4,5,6,7,8,9};
    BruteForceMedian bfm((int*) &testArray, 9);
    assert(5 == bfm.getMedian());
    return 0;
}



