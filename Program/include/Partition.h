#ifndef PARTITION_H
#define PARTITION_H


class Partition
{
    public:
        Partition(int* problemArray, unsigned int l, unsigned int h);
        ~Partition();
        int getPartition(void);
        long int getBasicOpCount(void);

    protected:

    private:
        // attribute(s)
        long int basicOperationCounter;
        int* problemArray;
        unsigned int l;
        unsigned int h;
        // method(s)
        void swapElements(unsigned int pos1, unsigned int pos2);
};

#endif // PARTITION_H
