#ifndef BRUTEFORCEMEDIAN_H
#define BRUTEFORCEMEDIAN_H

#include <cmath>

class BruteForceMedian
{
    public:
        BruteForceMedian(int* problemArray, long unsigned int arraySize);
        ~BruteForceMedian();
        int getMedian(void);
        long int getBasicOpCount(void);

    protected:

    private:
        long int basicOperationCounter;
        int* problemArray;
        long unsigned int problemSize;
};

#endif // BRUTEFORCEMEDIAN_H
