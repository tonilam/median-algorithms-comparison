#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <time.h>
#include <cmath>
#include <climits>

#include "BruteForceMedian.h"
#include "UnitTest.h"

using namespace std;

const char *LOG_FILE_FORMAT1 = "s_bfm_%d.txt";
const char *LOG_FILE_FORMAT2 = "s_mdn_%d.txt";
const int MAX_TICKS = 1000000;
const int SAMPLE_SIZE = 20;

void generateRandomArray(int* randomArray, long int arraySize);
void inspectAlgorithm(long int problemSize);
bool testFunction(void);
void printArray(string title, int* arrayToPrint, long int arraySize);

int timeUsed1 = 0;
int timeUsed2 = 0;
long int operationExecuted1 = 0;
long int operationExecuted2 = 0;

int main() {
    srand(clock());
    ofstream mylog1;
    ofstream mylog2;
    char *logfile1 = new char[20];
    char *logfile2 = new char[20];


    cout << "Try testing the algorithm functions before actual experiment." << endl;

    if (testFunction()) {

        for (int sampleIndex = 0; sampleIndex < SAMPLE_SIZE; ++sampleIndex) {
            cout << "PROCESSING SAMPLE[" << sampleIndex << "]" << endl;
            std::sprintf(logfile1, LOG_FILE_FORMAT1, sampleIndex);
            std::sprintf(logfile2, LOG_FILE_FORMAT2, sampleIndex);
            mylog1.open(logfile1);
            mylog2.open(logfile2);
            timeUsed1 = 0;
            timeUsed2 = 0;
            operationExecuted1 = 0;
            operationExecuted2 = 0;
            for (int i = 0;
                 ((timeUsed1 < MAX_TICKS) && (timeUsed2 < MAX_TICKS)
                  && (operationExecuted1 >= 0) && (operationExecuted2 >= 0)
                  && (i <= 60));
                 i++) {
                long int problemSize = 1000 + (i * 1000);
                inspectAlgorithm(problemSize);

                // record only the meaningful data
                if ((operationExecuted1 >= 0) && (operationExecuted2 >= 0)) {
                    mylog1 << problemSize << "\t" << operationExecuted1 << "\t" << timeUsed1
                           << endl;
                    mylog2 << problemSize << "\t" << operationExecuted2 << "\t" << timeUsed2
                           << endl;
                }
            }

            mylog1.close();
            mylog2.close();
        }

    } else {
        cout << "Tests not pass!" << endl;
    }

    return 0;
}

void generateRandomArray(int* randomArray, long int arraySize) {
    for (int i = 0; i < arraySize; ++i) {
        randomArray[i] = rand() % INT_MAX;
    }
}

void inspectAlgorithm(long int problemSize) {
        int arrayToProcess[problemSize] = {};
        generateRandomArray(arrayToProcess, problemSize);

        BruteForceMedian bfm((int*) arrayToProcess, problemSize);
        clock_t ticks = clock();
        bfm.getMedian();
        ticks = clock() - ticks;

        operationExecuted1 = bfm.getBasicOpCount();
        timeUsed1 = ticks;

        Median mdn((int*) arrayToProcess, problemSize);
        ticks = clock();
        for (int i = 0; i < 5; ++i) {
            mdn.getMedian();
        }
        ticks = clock() - ticks;

        operationExecuted2 = mdn.getBasicOpCount();
        timeUsed2 = ticks;
}

bool testFunction(void) {
    int testOrderedSample[] = {1,2,3,4,5,6,7,8,9};
    int testUnorderedSample[] = {9,1,2,6,4,8,5,3,7,10};
    int testBestCaseSample[] = {5,1,2,6,4,8,9,3,7,10};
    int testWorstCaseSample[] = {10,1,2,6,4,8,9,3,7,5};
    int testDuplicatedSample[] = {7,20,3,14,11,5,6,1,8,9,10,12,13,4,15,16,20,18,19,2};
    bool result_ut_bf,
         result_ut_bfop,
         result_ut_m,
         result_ut_mop;

    UnitTest ut(testOrderedSample, 9, 5,5);
    UnitTest ut1(testUnorderedSample, 10, 5, 6);
    UnitTest ut2(testBestCaseSample, 10, 5, 6);
    UnitTest ut3(testWorstCaseSample, 10, 5, 6);
    UnitTest ut4(testDuplicatedSample, 20, 10, 11);

    cout << "Unit Test for BruteForceMedian... ";
    result_ut_bf = ut.testBruteForceMedian()
                   && ut1.testBruteForceMedian()
                   && ut2.testBruteForceMedian()
                   && ut3.testBruteForceMedian()
                   && ut4.testBruteForceMedian();
    if (result_ut_bf){
        cout << "passed.";
    } else {
        cout << "failed.";
    }
    cout << "\n\t(Expected: 5, 6, 6, 6, 11;\n\t actual: "
         << ut.getBruteForceMedian() << ", " << ut1.getBruteForceMedian() << ", "
         << ut2.getBruteForceMedian() << ", " << ut3.getBruteForceMedian() << ", "
         << ut4.getBruteForceMedian()
         << ")" << endl;

    cout << "Unit Test for BruteForceMedian get operation... ";
    result_ut_bfop = ut.testBruteForceMedianOpCounter();
    if (result_ut_bfop){
        cout << "passed." << endl;
    } else {
        cout << "failed." << endl;
    }

    cout << "Unit Test for Median... ";
    result_ut_m = ut.testMedian()
                  && ut1.testMedian()
                  && ut2.testMedian()
                  && ut3.testMedian()
                  && ut4.testMedian();
    if (result_ut_m){
        cout << "passed.";
    } else {
        cout << "failed.";
    }
    cout << "\n\t(Expected: 5, 6, 6, 6, 11;\n\t actual: "
         << ut.getMedian() << ", " << ut1.getMedian() << ", "
         << ut2.getMedian() << ", " << ut3.getMedian() << ", "
         << ut4.getMedian()
         << ")" << endl;

    cout << "Unit Test for Median get operation... ";
    result_ut_mop = ut.testMedianOpCounter();
    if (result_ut_mop){
        cout << "passed." << endl;
    } else {
        cout << "failed." << endl;
    }

    return result_ut_bf && result_ut_bfop && result_ut_m && result_ut_mop;
}

void printArray(string title, int* arrayToPrint, long int arraySize) {
    cout << title << endl;
    for (int i = 0; i < arraySize; ++i) {
        cout << arrayToPrint[i] << " ";
    }
    cout << endl;
}


