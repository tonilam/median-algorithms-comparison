#include "Select.h"
#include "Partition.h"

Select::Select(int* problemArray, int l, int m, int h) {
    this->problemArray = problemArray;
    this->basicOperationCounter = 0;
    this->init_l = l;
    this->init_m = m;
    this->init_h = h;
}

Select::~Select() {
    //dtor
}

int Select::selectMedian() {
    return recursiveSelect(this->init_l, this->init_m, this->init_h);
}

int Select::recursiveSelect(int l, int m, int h) {
    // Returns the value at index m in array slice A[l..h], if the slice
    // were sorted into nondecreasing order.
    Partition pa(this->problemArray, l, h);
    int pos = pa.getPartition();

    this->basicOperationCounter += pa.getBasicOpCount();
    if (pos == m) {
        return this->problemArray[pos];
    }
    if (pos > m) {
        return recursiveSelect(l, m, pos - 1);
    }
    if (pos < m) {
        return recursiveSelect(pos + 1, m, h);
    }
    return -1;
}

long int Select::getBasicOpCount(void) {
    return this->basicOperationCounter;
}
