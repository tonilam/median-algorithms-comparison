#include "UnitTest.h"

UnitTest::UnitTest(int* testArray, unsigned long int arraySize, int answer_bfm, int answer_mdn) {
    this->testArray = testArray;
    this->arraySize = arraySize;
    this->answer_bfm = answer_bfm;
    this->answer_mdn = answer_mdn;

    // Generate test result for the two algorithms.
    BruteForceMedian bfm((int*) this->testArray, this->arraySize);
    Median mdn((int*) this->testArray, this->arraySize);
    this->result_bfm = bfm.getMedian();
    this->result_mdn = mdn.getMedian();
    this->bo_bfm = bfm.getBasicOpCount();
    this->bo_mdn = mdn.getBasicOpCount();
}

UnitTest::~UnitTest() {
    //dtor
}

int UnitTest::getBruteForceMedian() {
    return this->result_bfm;
}

int UnitTest::getMedian() {
    return this->result_mdn;
}

bool UnitTest::testBruteForceMedian(void) {
    return this->answer_bfm == this->result_bfm;
}

bool UnitTest::testBruteForceMedianOpCounter(void) {
    int arraySingleElement[] = {1};
    int array3Elements[] = {1,2,3};

    BruteForceMedian bfm1((int*) arraySingleElement, 1);
    BruteForceMedian bfm3((int*) array3Elements, 3);
    bfm1.getMedian();
    bfm3.getMedian();
    return (bfm1.getBasicOpCount() == 1) && (bfm3.getBasicOpCount() > 1);
}

bool UnitTest::testMedian(void) {
    return this->answer_mdn == this->result_mdn;
}

bool UnitTest::testMedianOpCounter(void) {
    int arraySingleElement[] = {1};
    int array3Elements[] = {1,2,3};

    Median mdn1((int*) arraySingleElement, 1);
    Median mdn3((int*) array3Elements, 3);
    mdn1.getMedian();
    mdn3.getMedian();
    return (mdn1.getBasicOpCount() == 1) && (mdn3.getBasicOpCount() > 1);
}

