#ifndef SELECT_H
#define SELECT_H


class Select
{
    public:
        Select(int* problemArray, int l, int m, int h);
        ~Select();
        int selectMedian(void);
        long int getBasicOpCount(void);

    protected:

    private:
        int recursiveSelect(int l, int m, int h);
        long int basicOperationCounter;
        int* problemArray;
        int init_l,
            init_m,
            init_h;
};

#endif // SELECT_H
