#include "Median.h"
#include "Select.h"

Median::Median(int* problemArray, long unsigned int arraySize) {
    this->problemArray = problemArray;
    this->basicOperationCounter = 0;
    this->problemSize = arraySize;
}

Median::~Median() {
    //dtor
}

int Median::getMedian(void) {
    int result = -1;
    if (this->problemSize == 1) {
        result = this->problemArray[0];
        this->basicOperationCounter = 1;
    } else {
        Select rs((int*) this->problemArray,
                  0,
                  floor(this->problemSize / 2.0),
                  this->problemSize - 1);
        result = rs.selectMedian();
        this->basicOperationCounter += rs.getBasicOpCount();
    }

    return result;
}

long int Median::getBasicOpCount(void) {
    return this->basicOperationCounter;

}
