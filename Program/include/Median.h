#ifndef MEDIAN_H
#define MEDIAN_H

#include <math.h>

class Median
{
    public:
        Median(int* problemArray, long unsigned int arraySize);
        ~Median();
        int getMedian(void);
        long int getBasicOpCount(void);

    protected:

    private:
        long int basicOperationCounter;
        int* problemArray;
        long unsigned int problemSize;
};

#endif // MEDIAN_H
