#include "Partition.h"

Partition::Partition(int* problemArray, unsigned int l, unsigned int h) {
    this->problemArray = problemArray;
    this->basicOperationCounter = 0;
    this->l = l;
    this->h = h;
}

Partition::~Partition() {
    //dtor
}

int Partition::getPartition(void) {
    // Partitions array slice A[l..h] by moving element A[l] to the position
    // it would have if the array slice was sorted, and by moving all
    // values in the slice smaller than A[l] to earlier positions, and all values
    // larger than or equal to A[l] to later positions. Returns the index at which
    // the ��pivot�� element formerly at location A[l] is placed.

     // Choose first value in slice as pivot value
    int pivotval = this->problemArray[this->l];
     // Location to insert pivot value
    int pivotloc = this->l;

    for (unsigned int j = this->l + 1; j <= this->h; ++j) {
        if (this->problemArray[j] < pivotval) {
            ++pivotloc;

            // Swap elements around pivot
            swapElements(pivotloc, j);
        }

        ++this->basicOperationCounter;
    }

    // Put pivot element in place
    swapElements(this->l, pivotloc);

    return pivotloc;
}

void Partition::swapElements(unsigned int pos1, unsigned int pos2) {
    int temp = 0;

    temp = this->problemArray[pos1];
    this->problemArray[pos1] = this->problemArray[pos2];
    this->problemArray[pos2] = temp;
}

long int Partition::getBasicOpCount(void) {
    return this->basicOperationCounter;
}
